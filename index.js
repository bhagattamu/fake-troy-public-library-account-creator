const express = require('express');
const { PORT } = require('./constants');
const apiController = require('./src/Controller/apiController');

const app = express();

app.use('/', express.static('public'));

app.use('/api', apiController);

app.listen(PORT, (err) => {
    if(err){
        throw err;
    };
    console.log(`Server running in PORT: ${PORT}`);
});
