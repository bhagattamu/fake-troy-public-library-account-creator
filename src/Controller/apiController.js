const { Router } = require('express');
const https = require('https');
const fs = require('fs');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');

const router = Router();

router.get('/get-fake-lynda-account', async (req, res) => {
    let dataObj = {};
    https.get("https://www.fakeaddressgenerator.com/usa_address_generator", getRes => {
        let html = "";
        getRes.on("data", (chunk) => {
            html += chunk;
        })
        getRes.on("end", () => {
            //console.log("Response finished", html);
            const $ = cheerio.load(html);
            //console.log($);
            //console.log($('.table,.common-table').html());
            //console.log($('.table,.common-table').children()['0'].children)
            
            $('.table,.common-table').children()['0'].children.map(tr => {
                let key = '';
                let value = '';
                tr.children.map(td => {
                    td.children.map(span => {
                        if(key == ''){
                            key = span.children[0].data;
                        }else{
                            value = span.children[0].data;
                        }
                    })  
                })
                dataObj[key] = value;
            })
            //console.log("Find span" ,$('.row .item').find('span')['0'])
            let arrKey = [];
            let arrValue = [];
            $('.row .item').find('span').map((i,span) => {
                if(i > 10){
                    return;
                }
                arrKey.push(span.children[0].data == undefined ? span.children[0].children[0].data : span.children[0].data);
                //console.log("child",span.children[0]);
                
            })
            $('.row .item').find('input').map((i,input) => {
                if(i > 10){
                    return;
                }
                // console.log(input)
                arrValue.push(input.attribs.value);
                //console.log("input",$('.row .item').find('input')[input].attribs.value);
            })
            arrKey.forEach((key, i) => {
                dataObj[key] = arrValue[i];
            })
            console.log(dataObj);
            // We have fake data in dataObj
            /**
             * Now using puppeteer to signup in detroy library
             */
            (async () => {
                const browser = await puppeteer.launch({args: ['--no-sandbox']});
                await signupLibrary(browser);
                const page = await browser.newPage();
                await page.goto('https://catalog.library.troymi.gov/polaris/patronaccount/selfregister.aspx?ctx=1.1033.0.0.1');
                await page.select('#lstBranches', '3');
                await page.type('#txtZipCode', dataObj['Zip Code']);
                await Promise.all([
                    page.waitForNavigation(),
                    page.click('#btnSubmitBranchAndZip')
                ])
                const isFound = await page.$('#lstCityAddresses').then(res => !!res).catch(e => console.log(e))
                console.log("Is found:",isFound);
                if(isFound){
                    await browser.close();
                    res.json({
                        message: "Try again"
                    })
                }else{
                    const isFirstNameFound = await page.$('#firstNameEntry').then(res => !!res).catch(e => console.log(e));
                    if(!isFirstNameFound){
                        console.log("not found");
                        res.json({
                            message: "Try again"
                        })
                        return;
                    }
                    await page.type('#firstNameEntry', dataObj['Full Name'].split(/\s+/)[0])
                    await page.type('#lastNameEntry', dataObj['Full Name'].split(/\s+/)[2] !== undefined ? dataObj['Full Name'].split(/\s+/)[2] : dataObj['Full Name'].split(/\s+/)[1])
                    //console.log('fullName:',dataObj['Full Name'].split(/\s+/));
                    await page.select('#DropdownGenderIDs', dataObj.Gender == 'male' ? '3' : '2')
                    // console.log(dataObj.Birthday.split('/'))
                    await page.type('input[name="txtBirthdatemm"]', dataObj.Birthday.split('/')[0])
                    await page.type('input[name="txtBirthdatedd"]', dataObj.Birthday.split('/')[1])
                    await page.type('input[name="txtBirthdateyyyy"]', dataObj.Birthday.split('/')[2])
                    await page.type('input[name="txtStreet1"]', dataObj.Street)
                    /**
                     * For fake email 
                     */
                    const fakeEmailPage = await browser.newPage();
                    await fakeEmailPage.goto('https://www.fakemail.net/');
                    const fakeEmail = await fakeEmailPage.evaluate('document.querySelector("#email").innerText');
                    await fakeEmailPage.close();
                    await page.type('input[name="txtEmail"]', fakeEmail);
                    await page.type('input[name="txtUsername"]', fakeEmail.split('@')[0]);
                    await page.type('input[name="txtPassword"]', '4444');
                    await page.type('input[name="txtVerification"]', '4444');
                    await Promise.all([
                        page.waitForNavigation(),
                        page.click('#btnSubmitMainForm')
                    ])
                    await page.screenshot({path: 'example.png'});
                    await browser.close();
                    res.json({
                        message: "Success",
                        username: fakeEmail.split('@')[0],
                        password: '4444'
                    })
                    return;
                }
                
                
            })()
            // res.json({
            //     ...dataObj
            // })
        })
    })
})

async function signupLibrary(browser){
    
}

module.exports = router;